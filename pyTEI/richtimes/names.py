from lxml import etree
from lxml.builder import E
import StringIO, os, glob
"""
This is mostly a testament to my own obtuseness

I'm going to do the same thing tomorrow, with way less headache,
using a totally different approach.

I put it up now so that I have a safe copy for reference, because I'm
going to be taking it apart.


collection @id
  text @id @date
    textNames
      name @authname

names
  name @authname @count
    occurs @collectionid @textid @div1type @div2type @div3type @div3n

get metadata tree
find all collections in tree
  each: find all texts in collection
    each: make tree from text ID; find all names in text
      each: make occurrence; name in names? append occurrence to name : make name, append occurrence 
            name in textNames? pass : append @authname to textNames
"""

base_path = '/Users/trevorborg/Reference/texts/'
class NameGlom:
    def __init__(self, metadata_source):
        """loads the metadata file you pass it on init. 
        - Collections is an array of document[@id starts with Perseus:collection]
        - the metadata tree is stored in self.metadata"""
        self.names = {}
        self.authnames = set()
        self.metadata = self.get_tree(metadata_source)
        self.collections = Matchers().select_collections(self.metadata)

    def collection_names_xml(self, collection_doc):
        """outputs an xml file:
        ``<collection id="collectionID">
             <text id="textID">
                 <name id="authname">"""
        collection = Collection(collection_doc)
        collection.texts = self.texts_in_collection(collection)
        for text in collection.texts:
            self.names_in_text(collection, text, self.get_tree(text.filename))
        collection.write_xml()
        
    def get_all_names(self):
        for c in Matchers().select_collections(self.metadata):
            collection = Collection(c)
            for text in self.texts_in_collection(collection):
                self.names_in_text(collection, text, self.get_tree(text.filename))
                collection.texts.append(text)
            self.collections.append(collection)
            print len(self.names.keys())

    def make_filename_from_id(self, id):
        return os.path.join(base_path, id.split(':')[2][0:7]+'/'+id.split(':')[2]+'.xml')

    def texts_in_collection(self, collection):
        texts = []
        for tID in Matchers().select_texts_in_collection(self.metadata, collection):
            filename = self.make_filename_from_id(tID)
            texts.append(Text(tID, filename))
        return texts

    def names_in_text(self, collection, text, tree):
        for persName in Matchers().select_names_in_text(tree):
            if 'authname' in persName.attrib.keys():
                authname = persName.attrib['authname']
                if len(authname.split(',')) > 1:
                    occur = Occurance(collection, text, persName)
                    if not authname in self.authnames:
                        self.names[authname] = Name(authname, occur)
                    else:
                        self.names[authname].occur(occur)
                    text.add_name(self.names[authname])
            else:
                pass
                    
    def get_tree(self, filename):
        with open(os.path.join(base_path, filename), 'r') as fin:
            return etree.parse(StringIO.StringIO(fin.read()))


#get_tree('RichTimes.metadata.xml')

class Matchers:
    def __init__(self):
        self.collections = ''.join(['/metadata',
                                    '/document',
                                    '[not(starts-with(@id, "Perseus:text")',
                                    ' or @id="Perseus:Collection:RichTimes")]'])
        self.texts_in_collection = ''.join(['/metadata/document[starts-with(@id, "Perseus:text")]',
                                            '/datum[@valueid="{0}"]/../@id'])
        self.occur_selector = ''.join(['/TEI.2/text/body/',
                                      '/div1[@type="{0}"]',
                                      '/div2[@type="{1}"]',
                                      '/div[3][@type="{2}" and @n="{3}"]',
                                      '/descendant::persName[@authname="{4}"]'])
        self.names_selector = '/TEI.2/text/body/div1/div2/div3/descendant::persName'

    def select_occur(self,tree, d1type, d2type, 
                     d3type, d4type, authname):
            return tree.xpath(self.occur_selector.format(d1type. d2type, 
                                                         d3type, d4type, authname))

    def select_collections(self, tree):
        return tree.xpath(self.collections)

    def select_texts_in_collection(self, tree, collection):
        return tree.xpath(self.texts_in_collection.format(collection.id))

    def select_names_in_text(self, text):
        return text.xpath(self.names_selector)

class Collection:
    def __init__(self, document):
        self.id = document.attrib['id']
        self.date = '-'.join(document.attrib['id'].split(':')[2].split('_')[1:2])
        self.texts = []

    def write_xml(self):
        col_e = E('collection', 
                  id=self.id,
                  date=self.date)
        for text in self.texts:
            col_e.append(text.make_element())
        with open(os.path.join(base_path, self.id.split(':')[2] + '.names.xml'), 'w') as fout:
            fout.write(etree.tostring(col_e, pretty_print=True))

class Text:
    def __init__(self, tID, filename):
        self.id = tID
        self.filename = filename
        self.textNames = {}

    def add_name(self, name):
        if not name.authname in self.textNames:
            self.textNames[name.authname] = name

    def findNames(self, bodyroot, colID, occurCounter):
        names = {}
        nset = set()
        counters = {'page': '1'}
        for child in bodyroot.iterchildren():
            if child.tag == 'milestone':
                counters[child.attrib['unit']] = child.attrib['n']
            else:
                div1 = child.attrib['type']
                for child2 in child.iterchildren():
                    if child2.tag == 'milestone':
                        for key in child2.attrib.keys():
                            counters[key] = child2.attrib[key]
                    elif child2.tag == 'div2' and 'type' in child2.attrib.keys(): 
                        div2 = child2.attrib['type']
                        for child3 in child2.iterchildren():
                            if child3.tag == 'div3' and 'type' in child3.attrib.keys():
                                div3t = child3.attrib['type']
                                if 'n' in child3.attrib.keys():
                                    div3n = child3.attrib['n']
                                else:
                                    div3n = '?'
                                for e in child3.iterdescendants():
                                    if e.tag == 'persName':
                                        if 'authname' in e.attrib.keys():
                                            if not e.attrib['authname'].isspace():
                                                authname = e.attrib['authname']
                                                occur = Occurance(occurCounter,
                                                                  colID, self.id, div1, 
                                                                  div2, div3t, div3n, counters)
                                                if not authname in nset:
                                                    names[authname] = Name(authname, occur)
                                                    nset.add(authname)
                                                else:
                                                    names[authname].occur(occur)
                                                occurCounter = occurCounter + 1
        return [names[authname] for authname in names.keys()]
                                
    def make_element(self):
        text_e = E('text', 
                   {
                       'id':self.id,
                   })
        for name in self.textNames.keys:
            text_e.append.name.make_element()
        return text_e

class Name:
    def __init__(self, authname, occur):
        self.authname = authname
        self.count = 0
        self.occurances = []
        self.occur(occur)

    def occur(self, occur):
        self.occurances.append(occur)
        self.count = self.count + 1

    def make_element(self):
        name_e = E('name',
                   {
                       'authname':self.authname,
                       'count':str(self.count)
                   })
        for occur in self.occurances:
            o_e = occur.make_element()
            name_e.append(o_e)
        return name_e
        

class Occurance:
    def __init__(self, occurCounter, colID, textID, d1type, d2type, d3type, d3n, counters):
        self.d1type = d1type
        self.d2type = d2type
        self.d3type = d3type
        self.d3n = d3n
        self.page = counters['page'] 
        self.column = counters['column']
        self.id = str(occurCounter)
        self.colID = colID
        self.textID = textID

    def make_element(self):
        return E('occur', 
                 {
                     'colID':self.colID,
                     'textID':self.textID,
                     'd1type':self.d1type,
                     'd2type':self.d2type, 
                     'd3type':self.d3type,
                     'd3n':self.d3n,
                     'page':self.page,
                     'column':self.column,
                     'id':self.id
                 })

a = NameGlom('RichTimes.metadata.xml')
a.occurCounter = 0
"""this big nasty script is all wrong, I know now.
It takes way too long. It works, but very inefficiently.

What I need to do, I'm thinking, is make a queue of names that is periodically
dumped to xml on disk.

So, you have a collection of names, which each hold a collection of occurrances.

Either a dump file for this name hasn't been created yet
in which case we need to make a new xml tree.

The file will be named by 'authname'[0]
so might as well grab all names starting with that letter now

if this is the first time, make a root node, 
and append each name.make_element(), then write it to disk

make sure all of the processed names are removed from memory

repeat process, matching the first letter of the next name
in the queue against the first letter of all names in the queue,
removing matches to a separate queue for storing
and then continuing with the next name until all 
occurring first-letters have been handled.

if the file already exists:
parse the tree, update it, write it, remove processed items from memory"""
for col in [Collection(col) for col in a.collections]:
    for t in a.texts_in_collection(col):
        t.finito = t.findNames(a.get_tree(t.filename)
                               .xpath('/TEI.2/text/body')[0], 
                               col.id, a.occurCounter)
        for name in t.finito:
            filename = 'names/'+name.authname[0]+'.names.xml'
            if glob.glob(os.path.join(base_path, filename)):
                names = a.get_tree(filename)
                authnames = [str(authname) for authname in names.xpath('/names/name/@authname')]
                if name.authname in authnames:
                    for n in names.getroot().iterchildren():
                        if n.attrib['authname'] == name.authname:
                            occurs = []
                            for o in n.iterchildren():
                                occurs.append(o.attrib['id'])
                            for occur in name.occurances:
                                if not occur.id in occurs:
                                    e = occur.make_element()
                                    n.append(e)
                                    n.attrib['count'] = str(len(n.getchildren()))
                else:
                    names.getroot().append(name.make_element())
                with open(os.path.join(base_path, filename), 'w') as fout:
                    fout.write(etree.tostring(names, pretty_print=True))
            else:
                names = E('names')
                e = name.make_element()
                names.append(e)
                with open(os.path.join(base_path, filename), 'w') as fout:
                    fout.write(etree.tostring(names, pretty_print=True))
        print t.filename
print 'si finito'


                







                
            
                        
            