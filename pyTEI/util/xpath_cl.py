#xpath_cl.py
"""
DEPENDENCIES: 
PyYaml : http://pypi.python.org/pypi/PyYAML/3.10
lxml : http://pypi.python.org/pypi/lxml/2.3.3

To make your command line experience really whiz-bang,
I recommend setting an alias for xpath_cl.py.

supported output formats include:

json
xml
yaml
plaintext (default)

$ python xpath_cl.py [opt -j/x/y] filename querystring [opt -o/n] [opt outfile name] [opt namespaces]

simplest usage:

$ python xpath_cl.py file.xml '//*'

(prints a plaintext representation of all of the elements in file.xml to the console)

more complex usage:

$ python xpath_cl.py -y file.xml '/a:foo/b:bar' -on outfile.xml 'a: http://www.codespeak.com/test1, b: http://www.codespeak.com/test2'

(output format specified to be yaml,
 response is appended to the file outfile.xml (or the file is created if it doesn't exist)
 the 'a' and 'b' namespaces are resolved with the given urls)

please note that if both -o and -n are present, 
Xpath_CL expects the outfile name before the namespace string.
"""
import sys, os, json, yaml
from lxml import etree
from StringIO import StringIO



class Xpath_CL:

    def __init__(self):
        self.expected_args = {'infile_name': 1, 
                              'querystring': 2,
                              'options': 3,
                              'outfile_name': 4,
                              'namespace_spec': 4}

    def bump_arg_index(self, n):
        for key in self.expected_args.keys():
            self.expected_args[key] = self.expected_args[key]+ n

    def set_output_format(self, arg):
        if arg[0] == '-':
            if  arg[1] == 'j':
                self.output_format = 'json'
            elif arg[1] == 'x':
                self.output_format = 'xml'
            elif arg[1] == 'y':
                self.output_format = 'yaml'
            return 1
        else:
            self.output_format = 'plaintext'
        return 0

    def set_namespaces(self, arg):
        namespaces = {}
        for key_val in arg.split(','):
            namespaces[key_val.split(': ')[0].strip()] = key_val.split(': ')[1].strip()
        return namespaces

    def get_node_sequence(self, filename, xpath_exp, namespaces={}):
        response = []
        with open(filename, 'r') as fin:
            xml_string_buffer = StringIO(fin.read())
            response = etree.parse(xml_string_buffer).xpath(xpath_exp, namespaces=namespaces)
        if self.output_format == 'json':
            response = self.convert_response_to_json(response)
        elif self.output_format == 'plaintext':
            response = self.convert_response_to_plaintext(response)
        elif self.output_format == 'xml':
            response = self.convert_response_to_xml(response)
        elif self.output_format == 'yaml':
            response = self.convert_response_to_yaml(response)
        return response

    def convert_response_to_json(self, response):
        if response:
            if isinstance(response[0], etree._ElementStringResult):
                return json.JSONEncoder().encode(response)
            else:
                elements = []
                for e in response:
                    element = {'tag': e.tag, 'attrib': dict(e.attrib), 'text': e.text}
                    elements.append(element)
                return json.JSONEncoder().encode(elements)

    def convert_response_to_yaml(self, response):
        if response:
            if isinstance(response[0], etree._ElementStringResult):
                outstrs = []
                for result in response:
                    outstrs.append(etree.tostring(result))
            else:
                elements = []
                for result in response:
                    element = {'tag': result.tag, 'attrib': dict(result.attrib), 'text': result.text}
                    elements.append(element)
                return elements
                    
    def convert_response_to_xml(self, response):
        outstr = '<response>'
        if response:
            if isinstance(response[0], etree._ElementStringResult):
                for result in response:
                    outstr = outstr +'<stringresult>' + result + '</stringresult>\n'            
            else:
                for result in response:
                    outstr = outstr + etree.tostring(result, pretty_print=True) + '\n'
        outstr = outstr + '</response>'
        return outstr

    def convert_response_to_plaintext(self, response):
        outstr = ''
        if response:
            if isinstance(response[0], etree._ElementStringResult):
                for string in response:
                    outstr = outstr + string + '\n'
            else:
                for e in response:
                    outstr_attribs = ''
                    if e.attrib.keys():
                        for key in e.attrib.keys():
                            outstr_attribs = outstr_attribs + key + ': '
                            outstr_attribs = outstr_attribs + e.attrib[key] + ', '
                    if outstr_attribs and e.text:
                        outstr = outstr + e.tag + ' ::@ ' + outstr_attribs + ':: ' + e.text + '\n'
                    elif outstr_attribs:
                        outstr = outstr + e.tag + ' ::@ ' + outstr_attribs + ':: none \n'
                    elif e.text:
                        outstr = outstr + e.tag + ' ::@ none :: ' + e.text + '\n'
                    else:
                        outstr = outstr + e.tag + ' ::@ none :: none \n'
        else:
            outstr = 'none'
        return outstr

    def output_to_file(self, infile_name, querystring, outfile, response):
        with open(outfile, 'a') as fout:
            #needs to be cleaned up
            header = '<queryResponses target="'+ infile_name + '" expression="' + querystring + ' output_format="' + self.output_format + '">\n'
            fout.write(header)
            if self.output_format == 'yaml':
                fout.write(yaml.dump(response))
            else:
                fout.write(response)
            fout.write('</queryResponses>\n')

    def output_to_console(self, infile_name, querystring, response):
        sys.stdout.write('RESPONSE :::::::::: '+infile_name+' :::::::::: '+querystring)
        sys.stdout.write('\n')
        if self.output_format == 'yaml':
            sys.stdout.write(yaml.dump(response))
        else:
            sys.stdout.write(response)

    def parse_sys_argv(self):
        self.bump_arg_index(self.set_output_format(sys.argv[1]))
        infile_name = sys.argv[self.expected_args['infile_name']]
        querystring = sys.argv[self.expected_args['querystring']]
        response = ''
        options = {}
        if len(sys.argv) > self.expected_args['options']:
            options = self.parse_options(sys.argv[self.expected_args['options']])
        if 'ns' in options.keys():
            response = self.get_node_sequence(infile_name, querystring, namespaces=options['ns'])
        else:
            response =  self.get_node_sequence(infile_name, querystring)
        if 'outfile' in options.keys():
            self.output_to_file(infile_name, querystring, options['outfile'], response)
        else:
            self.output_to_console(infile_name, querystring, response)

    def parse_options(self, options_string):
        options = {}
        if options_string[0] == '-':
            if 'o' in options_string:
                self.expected_args['namespace_spec'] = self.expected_args['namespace_spec'] + 1
                options['outfile'] = sys.argv[self.expected_args['outfile_name']]
            if 'n' in options_string:
                options['ns'] = self.set_namespaces(sys.argv[self.expected_args['namespace_spec']])
        return options
            

if __name__ == '__main__':
    if len(sys.argv) > 1:
        xp = Xpath_CL()
        xp.parse_sys_argv()
#EOF