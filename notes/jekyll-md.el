(defun jekyll-front ()
  (if (zerop (length (buffer-string)))
      (insert "---\nlayout:post\ntitle:\"\"\ncategories: []\ntags: []\n---\n")))

(defun jekyll-format ()
  (if (not (file-exists-p (buffer-file-name (current-buffer))))
      (write-file (concat (substring (pwd) 10) 
			  (format-time-string "%Y-%m-%d") "-"
			  (buffer-name (current-buffer))))))
(add-hook 'markdown-mode-hook 'jekyll-front)
(add-hook 'markdown-mode-hook 'jekyll-format)
