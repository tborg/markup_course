AN EXPLORATORY SESSION WITH LXML AND XPATH
==========================================
edited to the exclusion of my many errors
-----------------------------------------

<pre> 

      λ ~/Reference/texts/1999.04/ cat 1999.04.0100.metadata.xml

      <metadata> <document id="Perseus:text:1999.04.0100">
      <datum key="perseus:CorrectionLevel" lang="?">medium</datum><datum
      key="dc:Date:Copyrighted" lang="?">1888</datum><datum
      key="dc:Language" lang="?">en</datum><datum key="dc:Source"
      lang="?">Selections from the Attic Orators. Sir Richard
      C. Jebb. London. Macmillan. 1888.</datum><datum key="dc:Creator"
      lang="?">Sir Richard C. Jebb</datum><datum key="perseus:Citation"
      lang="?">orator:speech:section*</datum><datum key="perseus:Method"
      lang="?">professional data entry</datum><datum key="perseus:Funder"
      lang="?">The National Endowment for the Humanities</datum><datum
      key="dc:Title" lang="?">Selections from the Attic
      Orators</datum></document><document
      id="Perseus:text:1999.04.0100:orator=Antiphon"><datum
      key="perseus:Citation"
      lang="?">orator:speech:tetralogy:section*</datum></document></metadata>

      λ python

      Python 2.7.1 (r271:86832, Jul 31 2011, 19:30:53) [GCC 4.2.1 (Based on
      Apple Inc. build 5658) (LLVM build 2335.15.00)] on darwin Type "help",
      "copyright", "credits" or "license" for more information.
	
	>>> from lxml import etree
	
	>>> from StringIO import StringIO

	>>> f = open('1999.04.0100.metadata.xml', 'r')

	>>> f = StringIO(f.read())

	>>> tree = etree.parse(f)

	>>> it = tree.iter()

	>>> data = tree.xpath('//datum')

	>>> data

	[<Element datum at 0x10cdeb8c0>, <Element datum at 0x10cdeb910>, <Element datum at 0x10cdeb960>, <Element datum at 0x10cdeb9b0>, <Element datum at 0x10cdeba00>, <Element datum at 0x10cdeba50>]
	
	>>> for e in data:
	...     print e.xpath('attribute::key')
	... 
	
	['dc:Language']
	['dc:Source']
	['dc:Creator']
	['perseus:Citation']
	['perseus:Funder']
	['dc:Title']
	
	>>> for e in data:
	...     print e.xpath('attribute::key')+e.xpath('text()')
	... 
	
	['dc:Language', 'en']
	['dc:Source', 'Commentary on Thucydides Book 1. Charles D. Morris. 1891. Boston. Ginn and Company.']
	['dc:Creator', 'Charles D. Morris']
	['perseus:Citation', 'book:chapter*']
	['perseus:Funder', 'The National Endowment for the Humanities']
	['dc:Title', 'Commentary on Thucydides Book 1']
	
	>>> data[1].tag
	
	'datum'
	
	>>> data[1].text
	
	'Commentary on Thucydides Book 1. Charles D. Morris. 1891. Boston. Ginn and Company.'
	
	>>> data[1].attrib
	
	{'lang': '?', 'key': 'dc:Source'}
	
	>>> data[1].attrib['key']
	
	'dc:Source' 
	
	>>> for e in tree.iter():
	...     print e.tag
	... 
	
	metadata
	document
	datum
	datum
	datum
	datum
	datum
	datum
	
	>>> tree.xpath('metadata/document/datum')
	
	[]
	
	>>> tree.xpath('/metadata/document/datum')
	
	[<Element datum at 0x10cdeb8c0>, <Element datum at 0x10cdeb910>, <Element datum at 0x10cdeb960>, <Element datum at 0x10cdeb9b0>, <Element datum at 0x10cdeba00>, <Element datum at 0x10cdeba50>]
	
	>>> tree.xpath('//document/datum')
	
	[<Element datum at 0x10cdeb8c0>, <Element datum at 0x10cdeb910>, <Element datum at 0x10cdeb960>, <Element datum at 0x10cdeb9b0>, <Element datum at 0x10cdeba00>, <Element datum at 0x10cdeba50>]
	
	>>> a = tree.xpath('/metadata/document/datum')
	
	>>> a[0].attrib
	
	{'lang': '?', 'key': 'dc:Language'}
	
	>>> a[0].text
	
	'en'
	
	>>> for e in a:
	...     print e.attrib['key'], e.text
	... 
	
	dc:Language en
	dc:Source Commentary on Thucydides Book 1. Charles D. Morris. 1891. Boston. Ginn and Company.
	dc:Creator Charles D. Morris
	perseus:Citation book:chapter*
	perseus:Funder The National Endowment for the Humanities
	dc:Title Commentary on Thucydides Book 1
	
	>>> metadata = {}
	
	>>> for e in a:
	...     metadata[e.attrib['key']] = e.text
	... 
	
	>>> metadata
	
	{'dc:Source': 'Commentary on Thucydides Book 1. Charles D. Morris. 1891. Boston. Ginn and Company.', 'dc:Language': 'en', 'perseus:Funder': 'The National Endowment for the Humanities', 'dc:Title': 'Commentary on Thucydides Book 1', 'dc:Creator': 'Charles D. Morris', 'perseus:Citation': 'book:chapter*'}
	
	>>> metadata.keys()

	['dc:Source', 'dc:Language', 'perseus:Funder', 'dc:Title', 'dc:Creator', 'perseus:Citation']
	
	>>> metadata = {}
	
	>>> for e in a:
	...     metadata[e.attrib['key'].split(':')[1]] = e.text
	... 
	
	>>> metadata

	{'Language': 'en', 'Title': 'Commentary on Thucydides Book 1', 'Citation': 'book:chapter*', 'Source': 'Commentary on Thucydides Book 1. Charles D. Morris. 1891. Boston. Ginn and Company.', 'Creator': 'Charles D. Morris', 'Funder': 'The National Endowment for the Humanities'}
	
	>>> metadata.keys()
	
	['Language', 'Title', 'Citation', 'Source', 'Creator', 'Funder']
	
	>>> metadata['Citation']

	'book:chapter*'
	
</pre>